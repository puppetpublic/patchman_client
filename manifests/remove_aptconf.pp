# Make sure the file /etc/apt/apt.conf.d/05patchman is absent, Otherwise
# it sends report to the patchman server everytime when we install or
# upgrade a package.
class patchman_client::remove_aptconf {

  file { '/etc/apt/apt.conf.d/05patchman':
    ensure => absent,
  }

}
