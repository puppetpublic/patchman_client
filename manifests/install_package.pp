# Make sure the 'patchman-client' package is installed. We make this a
# class so that it can safely be loaded multiple times.
class patchman_client::install_package {
  package { 'patchman-client':
    ensure => 'present',
  }
}
