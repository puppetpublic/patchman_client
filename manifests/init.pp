# Create the configuration to send Patchman information to a Patchman server.
# This define:
#
# * ensures that the patchman-client is installed
# * downloads the secret needed to send the report to the Patchman server
# * creates a /etc/cron.daily file that send the report every day
#
# We make this a "define" rather than a "class" so that we can, if
# so choose, send to multiple Patchman servers.
#
# If using this define more than once, be sure that each
# $wallet_client_netrc parameter is distinct from any other or else you
# will get a duplicate resource error.
#
# NOTE: This define will _always_ install the patchman-client package, even
# if the $ensure parameter is set to 'absent'. This is due to the fact that
# if multiple patchman_client defines are used there is no way for one
# define to know if the others are ensuring 'absent' or 'present'. Thus,
# it is up to the user to remove the patchman-client themselves if they
# want it removed.

# ######################################################################
# $ensure: if set to 'present' will install the necessary confgurations
# for the Patchman server given by $name.

define patchman_client (
  Enum['present', 'absent'] $ensure = 'present',
  $wallet_client_netrc              = undef,
  Boolean $use_pepublic_wallet      = false,
)
{
  if ($wallet_client_netrc  == undef) {
    fail('wallet_client_netrc: must be set to download the patchman netrc')
  }

  # We define $patchman_server as an alias $name just to make
  # the rest of the code a little clearer.
  $patchman_server = $name

  # Install/remove the patchman_client package:
  include patchman_client::install_package

  # Get the .netrc file with the username and password. This is used to
  # login to the patchman service for uploads. We use $name as part of the
  # filename in case this define is called more than once.
  $netrc_path = "/etc/patchman/${patchman_server}.netrc"

  if ($use_pepublic_wallet) {
    wallet { $wallet_client_netrc:
      ensure  => $ensure,
      type    => 'file',
      path    => $netrc_path,
      owner   => 'root',
      group   => 'root',
      mode    => '0660',
      require => Class['Patchman_client::Install_package'],
    }
  } else {
    base::wallet { $wallet_client_netrc:
      ensure  => $ensure,
      type    => 'file',
      path    => $netrc_path,
      owner   => 'root',
      group   => 'root',
      mode    => '0660',
      require => Class['Patchman_client::Install_package'],
    }
  }

  $patchman_conf_path = "/etc/patchman/${patchman_server}.conf"
  file { $patchman_conf_path:
    ensure  => $ensure,
    content => template('patchman_client/etc/patchman/patchman-client.conf.erb'),
    require => Class['Patchman_client::Install_package'],
  }

  ## Make sure the file /etc/apt/apt.conf.d/05patchman is absent,
  ## otherwise it sends report to patchman server everytime when install
  ## or upgrade a package.
  include patchman_client::remove_aptconf

  # Install a cron job to update patchman periodically. For the name of
  # the cron job we use "patchman-client-<patchman_server>" but we change
  # all the dots (".") in <patchman-server> to underscores ("_"). We do
  # this because run-parts (which cron uses) will only run scripts whose
  # filenames match /^[a-zA-Z0-9_-]+$/. See the man page for run-parts for
  # more information.
  $patchman_server_rp = regsubst($patchman_server, '[.]', '_', 'G')
  file { "/etc/cron.daily/patchman-client-${patchman_server_rp}":
    ensure  => $ensure,
    content => template('patchman_client/etc/cron.daily/patchman-client.erb'),
    mode    => '0755',
    require => Class['Patchman_client::Install_package'],
  }

}
