# patchman_client

## Introduction

Module to install patchman client package and configuration.

    patchman_client { 'patchman.example.com':
      ensure               => 'present',
      wallet_client_netrc  => 'wallet_file_name',
    }

We make this a Puppet define to allow a server to send Patchman reports to
multiple Patchman servers. The define `name` (in this case,
`patchman.example.com`) is the fully-qualified name of the Patchman
server.

## The `.netrc` file

The `wallet_client_netrc` parameter is required and should be the Wallet
name for the netrc file used by the Patchman client when connecting to the
Patchman server. It should have this format:
```
machine patchman.example.com
login patchman-client
password vEWLKbvJdxUicT2meV5j
```

The login/password correspond to the Basic authentication used to access
the Patchman server's report upload service.

## Removing the `patchman-client` package

If the `patchman_client` define is called multiple times there is no way
for one define to know if the other defines are setting the `ensure`
parameter to `present` or `absent`. Because of this the
`patchman_client` define _always_ installs the `patchman-client`
package. If you need to remove the `patchman-client` package, you will
have to do so separately after removing all calls to the `patchman_client`
define from your Puppet code.
